//
rootProject.name = "idea-plugins-cookbook"
//
include("hello-from-module")
//
include("experimental-module")
//
include("colour-modules:blue-module")
include("colour-modules:cyan-module")
include("colour-modules:green-module")
include("colour-modules:magenta-module")
include("colour-modules:red-module")
include("colour-modules:white-module")
include("colour-modules:yellow-module")
//
