package tk.labyrinth.ideabox;

import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.editor.event.EditorFactoryEvent;
import com.intellij.openapi.editor.event.EditorFactoryListener;
import com.intellij.openapi.editor.ex.EditorEx;
import com.intellij.openapi.editor.ex.FocusChangeListener;
import org.jetbrains.annotations.NotNull;

import javax.annotation.Nullable;

public class EditorFocusService implements EditorFactoryListener, FocusChangeListener {

	private static EditorFocusService instance = null;

	private Editor focused = null;

	{
		instance = this;
	}

	@Override
	public void editorCreated(@NotNull EditorFactoryEvent event) {
		Editor editor = event.getEditor();
		if (editor instanceof EditorEx) {
			EditorEx editorEx = (EditorEx) editor;
			editorEx.addFocusListener(this);
		}
	}

	@Override
	public void focusGained(@NotNull Editor editor) {
		focused = editor;
	}

	@Override
	public void focusLost(@NotNull Editor editor) {
		focused = null;
	}

	@Nullable
	public Editor getFocusedEditor() {
		return focused;
	}

	public static EditorFocusService getInstance() {
		return instance;
	}
}
