package tk.labyrinth.ideabox;

import com.intellij.lang.java.JavaLanguage;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.module.ModuleUtil;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiDocumentManager;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.PsiMethod;
import com.intellij.psi.PsiModifier;
import com.intellij.psi.PsiType;
import com.intellij.psi.augment.PsiAugmentProvider;
import com.intellij.psi.impl.light.LightMethodBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class HelloFromModuleAugmentProvider extends PsiAugmentProvider {

	@NotNull
	@Override
	@SuppressWarnings("unchecked")
	protected <Psi extends PsiElement> List<Psi> getAugments(@NotNull PsiElement element, @NotNull Class<Psi> type) {
		List<Psi> result;
		if (type == PsiMethod.class) {
			PsiClass elementAsClass = (PsiClass) element;
			if (Objects.equals(elementAsClass.getQualifiedName(), Number.class.getCanonicalName())) {
				result = Stream.of((Psi) tryCreateYouAreAtModuleMethod(elementAsClass))
						.filter(Objects::nonNull)
						.collect(Collectors.toList());
			} else {
				result = List.of();
			}
		} else {
			result = List.of();
		}
		return result;
	}

	private static PsiMethod createYouAreAtModuleMethod(PsiClass cl, Module module) {
		LightMethodBuilder result = new LightMethodBuilder(cl.getManager(), JavaLanguage.INSTANCE,
				"youAreAt" + getFormattedModuleName(module));
		result.setContainingClass(cl);
		result.setMethodReturnType(PsiType.VOID);
		result.addModifiers(PsiModifier.FINAL, PsiModifier.PUBLIC);
		return result;
	}

	private static String getFormattedModuleName(Module module) {
		// Gradle Java format:
		// root-module.module.(main|test)
		//             ^ We take name from here.
		List<String> simpleNameChain = List.of(module.getName().split("\\."));
		return simpleNameChain.stream().skip(simpleNameChain.size() - 2)
				.flatMap(simpleName -> Stream.of(simpleName.split("-")))
				.map(word -> Character.toUpperCase(word.charAt(0)) + word.substring(1))
				.collect(Collectors.joining());
	}

	@Nullable
	private static PsiMethod tryCreateYouAreAtModuleMethod(PsiClass cl) {
		PsiMethod result;
		{
			Editor focusedEditor = EditorFocusService.getInstance().getFocusedEditor();
			if (focusedEditor != null) {
				PsiFile focusedFile = PsiDocumentManager.getInstance(cl.getProject())
						.getPsiFile(focusedEditor.getDocument());
				Module focusedModule = ModuleUtil.findModuleForFile(focusedFile);
				result = createYouAreAtModuleMethod(cl, focusedModule);
			} else {
				result = null;
			}
		}
		return result;
	}
}
