//
plugins {
	id("org.jetbrains.intellij").version("0.4.16")
	java
}
//
intellij {
	setPlugins("java")
	version = "2019.3.3"
}
//
