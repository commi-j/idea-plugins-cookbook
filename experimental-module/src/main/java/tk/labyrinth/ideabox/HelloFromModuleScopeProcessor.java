package tk.labyrinth.ideabox;

import com.intellij.psi.PsiElement;
import com.intellij.psi.ResolveState;
import com.intellij.psi.scope.PsiScopeProcessor;
import org.jetbrains.annotations.NotNull;

public class HelloFromModuleScopeProcessor implements PsiScopeProcessor {

	@Override
	public boolean execute(@NotNull PsiElement element, @NotNull ResolveState state) {
		return false;
	}
}
